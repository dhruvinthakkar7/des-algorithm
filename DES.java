import crypto.*;
import crypto.specs.*;
import java.util.Scanner;
import java.util.ArrayList;
public class DES extends DesSpecs implements Cryptography,Sboxes{
    	public DES(){}
    	public DES(String msg){
    			Scanner scanner = new Scanner(System.in);
				System.out.println("Enter plaintext in hexadecimal:");
				String plainText = scanner.nextLine();
				System.out.println("Enter key in 16-bit hexadecimal:");
				String key = scanner.nextLine();
				System.out.println("ENCRYPTED FORM: "+encryption(plainText,key));
    	}
    	/*Method Name - binary
    	  Return Type -String
    	  It takes a string as an argument and returns the equivalent binary
    	  If the length of the string is 2 first replace all A to F with respective decimals
    	  and then if the length is more than 2 then make pair of 2 digits and pass index to array
    	  Ex-AB after replacing ->10 11 and after replacing if A To F is not present in str 
    	  the length will remain as 2 and then check if str is more than 15 or not if it is then
    	  return the binary of each digit Ex-16 -> 0001 0110 and if str is less than 15 then 
    	  directly give it to array.If str's length is initially more than 2 then 
    	  return the binary of each digit*/

    	private String binary(String str){
			
			String str1="",str2="";
			int a=0;
			String [] arr={"0000","0001","0010","0011","0100","0101","0110","0111","1000","1001",
								"1010","1011","1100","1101","1110","1111"};

			if(str.length()<=2){
					str = str.replace("A","10").replace("B","11").replace("C","12")
						  .replace("D","13").replace("E","14").replace("F","15")
						  .replace("a","10").replace("b","11").replace("c","12")
						  .replace("d","13").replace("e","14").replace("f","15");

				if(str.length()>2){
							
							for(int i=0;i<str.length();i+=2){
								str2 += Character.getNumericValue(str.charAt(i));
								str2 += Character.getNumericValue(str.charAt(i+1));
								str1 += arr[Integer.parseInt(str2)];
								str2="";
						}			
					}
					else{
						if(Integer.parseInt(str)>15){
								for(int i=0;i<str.length();i++)
									str1 += arr[Character.getNumericValue(str.charAt(i))];			
						}
						else
							str1 += arr[Integer.parseInt(str)];
					}
			}
			else{
				for(int i=0;i<str.length();i++)
						str1 += arr[Character.getNumericValue(str.charAt(i))];
			}
			return str1;
		}
		/*Method Name - permutation
		  Return Type - String
		  It has 2 arguments namely an 2D array and String that is to be permuted acc 
		  to the array. The index of character which is specified at tobepermuted string 
		  is each element of the 2D array -1 because array begins with 0th index  
		*/

		private String permutation(int[][] array,String tobepermuted){
			String str1="";
			for(int i=0;i<array.length;i++){
				for(int j=0;j<array[0].length;j++){
						 str1 += tobepermuted.charAt(array[i][j]-1);
				}
			}
			return str1;
		}
		/*Method Name - rotate
		  Return Type - String array
		  It accepts a single argument i.e. a String array with its 0th index filled(c[0] and d[0])
		  and it returns the same array by filling rest of the indexes by rotating acc to the 
		  ROTATECON.
		*/

		private String[] rotate(String[] str){
			for(int i=0;i<DesSpecs.ROTATECON.length;i++){
    			StringBuilder sb = new StringBuilder(str[i]);
    			int j=0;
    			while(j<DesSpecs.ROTATECON[i]){
    				sb = sb.append(sb.charAt(0));
    				sb = sb.deleteCharAt(0);
    				j++;	
    			}	
    			str[i+1]=sb.toString();
    		}
    		return str;
		}
		/*Method Name - xor
		  Return Type - String
		  It accepts two strings and returns a new string by Xoring both the strings.
		  First convert each charcater of both the strings into int and then xor it by 
		  using bitwise operator(^)EX- character at 0th index of first string xor
		  with character at 0th index of second string .
		*/
		private String xor(String str1,String str2){
			String str3="";
			for(int i=0;i<str2.length();i++){
    			int a = (int)str1.charAt(i);
    			int b = (int)str2.charAt(i);
    			str3+=a^b;
    		}
    		return str3;
		}
		/*Method Name - divideInParts
		  Return Type - String array
		  It accepts 3 args namely a string ,group of bits in one part and size of parts
		  and returns a String[] by dividing str in  partSize each of grp of bits. 
		*/
		private String[] divideInParts(String str,int grp,int partSize){
			String[] str1 = new String[partSize];
			int k=0;
			for(int i=0;i<str1.length;i++)
					str1[i]="";
			for(int j=0;j<str1.length;j++){
				for(int i=k;i<k+grp;i++)
					str1[j]+=str.charAt(i);
						k+=grp;
			}
			return str1;
		}
		/*Method Name - mainFunction
		  Return Type - String
		  It accepts two strings (r,k[i]).This function is internally invoked by 
		  swapString method. It does expansion acc to expansion table on str
		  then xoring is done between expanded str and str2.The answer is divided by invoking
		  divideInParts method into 8 parts each of 6 bits.Then substitution is done acc to 
		  sbox 3D array .The answer is then permuted acc to FINALPERCON Constant and the str
		  is returned.
		*/
		private String mainFunction(String str,String str2){
			return permutation(DesSpecs.FINALPERCON,substitution(divideInParts(xor(str2,permutation(DesSpecs.EXPANSIONCON,str)),6,8)));
		}
		/*Method Name - substitution
		  Return Type - String
		  It accepts a String array which is divided in parts and returns the substituted string
		  acc to sbox 3D array
		*/
		private String substitution(String[] B){
			String str="";
			for(int i=0;i<B.length;i++){
    		 	String a="",b="";
    		 	 a+=B[i].charAt(0);
    		 	 a+=B[i].charAt(5);
    		 	 for(int j=1;j<5;j++)
    		  	 		b += B[i].charAt(j);
    		   str += binary(Integer.toString(Sboxes.S[i][Integer.parseInt(a,2)][Integer.parseInt(b,2)]));
    		     
    		 }
    		 return str;
		}
		/*Method Name- swapString
		  Return Type -String
		  It accepts a string ,string and a String[]. We provide l1 and r1 and the method returns
		  l16 r16 in a swapped manner. 
		*/
		private String swapString(String l,String r,String[] k){
				String str="";
				for(int i=0;i<16;i++){
					str = r;
					r = xor(l,mainFunction(r,k[i]));
					l = str;
				}
			String swapstr = r+l;
			return swapstr;
		}
		/*Method Name - binaryToHex
		  Return Type - String
		  It accepts a binary string and makes pair of 4 digits and returns equivalent 
		  hexadecimal form of the pair.
		*/
		private String binaryToHex(String s){
			String str="";
			int k=0;
			String[] str1 = divideInParts(s,4,16);
			for(int i=0;i<str1.length;i++)
					str +=Integer.toHexString(Integer.parseInt(str1[i],2));
			return str;
		}
		private String[] encryptKey(String key){
			String[] c = new String[17];
			String[] d = new String[17];
			String[] k = new String[16];
			for(int i=0;i<17;i++){
				c[i]="";
				d[i]="";
			}
			String str="";
			String keyinbinary = binary(key); 
			String pc1 = permutation(DesSpecs.PCONECON,keyinbinary);
			c[0] = pc1.substring(0,28);
			d[0] = pc1.substring(28);
			c = rotate(c);
			d = rotate(d);
			for(int i=0;i<16;i++){
					 k[i] = "";
					 str = c[i+1]+d[i+1];
					 k[i] = permutation(DesSpecs.PCTWOCON,str);
			}
			return k;
		}
		private String encryption(String plainText,String key){
			String s2 = "",res="";
			ArrayList<String> ans = new ArrayList<>(); 
			if(plainText.length()%16!=0){
				plainText+="0D0A";
					while(plainText.length()%16!=0)
								plainText+="0";
			}
			String plaininbinary = binary(plainText);
			int len=plaininbinary.length()/64;
			for(int i=0;i<len;i++)
					ans.add(i,"");
			int m=0,j=0,dummylen;
			if(len>5)
				dummylen = 5;
			else
				dummylen = len;
			MultiThreading[] ml = new MultiThreading[5];
			for(j=0;j<dummylen;j++){
					for(int l=m;l<m+64;l++)
						 s2 += plaininbinary.charAt(l);
			ml[j] = new MultiThreading(s2,key);
			m+=64;
    		s2="";
   		}
   		int a=0,k=0,f=5,n,g=0;
   		boolean flag = false;
   		int l=j,b=1,c=2,d=3,e=4;
   		do{
   		switch(l){
   			case 1:	while(ml[a].t.isAlive());
   						break;
   			case 2: while(ml[a].t.isAlive() && ml[b].t.isAlive());
   						break;
   			case 3: while(ml[a].t.isAlive() && ml[b].t.isAlive() && ml[c].t.isAlive());
   						break;
   			case 4: while(ml[a].t.isAlive() && ml[b].t.isAlive() && ml[c].t.isAlive() 
   						&& ml[d].t.isAlive());
   						break;
   			case 5: while(ml[a].t.isAlive() && ml[b].t.isAlive() && ml[c].t.isAlive() 
   						&& ml[d].t.isAlive() && ml[e].t.isAlive());
   						break;
   		}
   		for(int i=0;i<j;i++){
   					if(ml[i].t.getState()==Thread.State.TERMINATED){
   						ans.add(i,ml[i].cipherText);
   						if(len>5 && f<len){
   							for(n=m;n<m+64;n++)
						 				s2 += plaininbinary.charAt(n);
						 	ml[i] = new MultiThreading(s2,key);
						 	if(f==len-1){}
						 	else
						 		m=n;
						 	switch(k){
									case 0: a=i;
											break;
									case 1: b=i;
											break;
									case 2: c=i;
											break;
									case 3: d=i;
											break;
									case 4: e=i;
											break;
							}
							System.out.println(a);
						 	k++;
						 	f++;
						 	s2="";
						}
				}
				else{
						switch(k){
								case 0: a=i;
										break;
								case 1: b=i;
										break;
								case 2: c=i;
										break;
								case 3: d=i;
										break;
								case 4: e=i;
										break;
						}
						k++;
					}
   			}
   			l=k;
   			k=0;
   		}while(l>0);
   		for(int i=0;i<len;i++)
   					res+=ans.get(i);
   		return res;
	}
		public String decrypt(String str1,String str2){return "Hi";}
		/*Method Name - encrypt
		  Return Type - String
		  It acceps plainText and key ,makes use of the private methods and returns the 
		  Cipher Text..
		*/ 
		public String encrypt(String plainText,String key){

			String[] k = encryptKey(key);
			String ip="",l0="",r0="",ans="",ipInverse="",swap="";
			ip = permutation(DesSpecs.IPCON,plainText);
			l0 = ip.substring(0,32);
			r0 = ip.substring(32);
			swap = swapString(l0,r0,k);
			ipInverse = permutation(DesSpecs.INVERSEIPCON,swap);
			ans += binaryToHex(ipInverse);
    	return ans;
	}
}
