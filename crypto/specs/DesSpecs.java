package crypto.specs;
public class DesSpecs{
		 protected static final int[][] PCONECON,PCTWOCON,IPCON,EXPANSIONCON,FINALPERCON,INVERSEIPCON;
		 protected static final int[] ROTATECON;
		static{
			PCONECON = generatePc1();
			ROTATECON = generateRotate();
			PCTWOCON = generatePc2();
			IPCON = generateIP();
			EXPANSIONCON = generateExp();
			FINALPERCON = generateFinalIP();
			INVERSEIPCON = generateInverseIP();
		}
		private static int[][] generatePc1(){
			return DesSpecsin.PCONECON;
		}
		private static int[] generateRotate(){
			return DesSpecsin.ROTATECON;
		}
		private static int[][] generatePc2(){
			return DesSpecsin.PCTWOCON;
		}
		private static int[][] generateIP(){
			return DesSpecsin.IPCON;
		}
		private static int[][] generateExp(){
			return DesSpecsin.EXPANSIONCON;
		}
		private static int[][] generateFinalIP(){
			return DesSpecsin.FINALPERCON;
		}
		private static int[][] generateInverseIP(){
			return DesSpecsin.INVERSEIPCON;
		}   
}
