package crypto;
public interface Cryptography {

  		 String encrypt(String plainText,String key);

  		 String decrypt(String cipherText,String key);   
}
