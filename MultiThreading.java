public class MultiThreading implements Runnable{
    String plainText,k,cipherText;
    Thread t;
    MultiThreading(String plainText,String k){
    	this.plainText = plainText;
    	this.k = k;
    	t = new Thread(this);
    	t.start();
    }
    public void run(){
    	cipherText = new DES().encrypt(plainText,k);
    }
}
